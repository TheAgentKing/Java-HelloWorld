package com.dah5.theagentking.helloworld;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

public class HelloWorld {

	private String message;
	
	public HelloWorld(String message) {
		this.message = message;
	}
	
	/**
	 * Download text from a URL String.
	 * @param url String
	 * @return String
	 * @throws IOException 
	 * @throws MalformedURLException 
	 */
	public String download(String url) throws Exception {
		return this.download(new URL(url));
	}
	
	/**
	 * Download text from a URL Object.
	 * @param url URL
	 * @return String
	 * @throws IOException 
	 */
	public String download(URL url) throws Exception {
		InputStream in = url.openStream();
		String output = "";
		String line = "";
		BufferedReader br = new BufferedReader(new InputStreamReader(in));
		while((line = br.readLine()) != null) {
			output += line;
		}
		br.close();
		in.close();
		return output;
	}
	
	/**
	 * Get the message currently set.
	 * @return String message
	 */
	public String get() {
		return this.message;
	}
	
	/**
	 * Set the message.
	 * @param message String
	 */
	public void set(String message) {
		this.message = message;
	}
	
	/**
	 * Prints out the message to the console.
	 */
	public void print() {
		System.out.println(this.message);
	}
	
	public static void main(String[] args) {
		// Create HelloWorld Object with Starting Message
		HelloWorld hw = new HelloWorld("Hello, world!");
		// Print that message to the console.
		hw.print();
		// Change the message.
		hw.set("Hello, King!");
		// Print it to the console.
		hw.print();
		// Try to fetch message from remote location.
		try {
			// Download the message
			String remoteMessage = hw.download("https://api.theagentking.dah5.com/helloworld.txt");
			// Change the message in the HelloWorld Object
			hw.set(remoteMessage);
			// Print the message to the console.
			hw.print();
		} catch (Exception e) {
			// Any problems during our fetch attempt, print the stack trace to the console.
			e.printStackTrace();
		}
		// Done :-]
	}
	
}
